<?php

require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCommandes.php';
require_once '../src/model/DAOInscrit.php';
require_once '../src/model/DAOKit.php';
require_once '../src/model/DAOLigneCommande.php';
require_once '../src/model/DAORole.php';
    
    
    

class AdministrateurController {
    
    
    public function display() {
        session_start();
        $daoinscrit=new DAOInscrit();
        //affichage des kits crée par le fabriquand connecté
        
        $inscrits=$daoinscrit->findAll();
        $page= Renderer::render('AccueilAdmin.php', compact('inscrits'));
        echo $page;
    }
   
    public function new(){
        $page= Renderer::render('newinscrit.php');
        echo $page;
    }
    
    public function savenew(){
        $daoinscrit=new DAOInscrit();
        $inscrit=new Inscrit();
        $vide=false;
        
        $_nom = htmlspecialchars(isset($_POST['nom']) ? $_POST['nom'] : NULL);
        if ($_nom==NULL){
            $_nom="";
            $vide=true;
        }
        $inscrit->setLogin($_nom);
        
        $mdp = htmlspecialchars(isset($_POST['mdp']) ? $_POST['mdp'] : NULL);
        if ($mdp==NULL){
            $mdp="";
            $vide=true;
        }
        $inscrit->setMdp($_nom);
        
        $role=$_POST['role'];
        $inscrit->setIdRole((int)$role);

        if($vide==false){
            $daoinscrit->save($inscrit);

            header('Location: /admin/accueil');
            exit();
        }
        else{
            
            header('Location: /admin/nouveau');
        }
    }
    
    public function modify($id){
        $daoinscrit = new DAOInscrit();
        $moninscrit = new Inscrit();
        $moninscrit = $daoinscrit->find($id);
        
        $page= Renderer::render('updateinscrit.php', compact('moninscrit'));
        echo $page;
    }
    
    public function saveupdate($id){
        $daoinscrit = new DAOInscrit();
        $inscrit=new Inscrit();
        $inscrit=$daoinscrit->find($id);
        
        $login = htmlspecialchars(isset($_POST['nom']) ? $_POST['nom'] : NULL);
        if ($login!=NULL){
            $inscrit->setLogin((string)$login);
        }
        
        $mdp = htmlspecialchars(isset($_POST['mdp']) ? $_POST['mdp'] : NULL);
        if ($mdp!=NULL){
            $inscrit->setMsp((string)$mdp);
        }
        
        $modifier=null;
        //todo mettre dans $modifier la valeur du bouton
        if ($modifier==true){
            $role=$_POST['role'];
            $inscrit->setIdRole((int)$role);
        }
        
        $daoinscrit->update($inscrit);
        header('Location: /admin/accueil');
        exit();
    }
    
    public function delete($id){
        $daoinscrit = new DAOInscrit();
        $daoinscrit->remove($id);
        header('Location: /admin/accueil');
    }
    
    
}