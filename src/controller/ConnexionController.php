<?php

require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCommandes.php';
require_once '../src/model/DAOInscrit.php';
require_once '../src/model/DAOKit.php';
require_once '../src/model/DAOLigneCommande.php';
require_once '../src/model/DAORole.php';
require_once '../src/model/Session.php';

class ConnexionController {

    public function display() {

        $page = Renderer::render('connexion.php');
        echo $page;
    }

    public function newconnexion() {
        $login = htmlspecialchars(isset($_POST["login"]) ? $_POST["login"] : NULL);
        $mdp = htmlspecialchars(isset($_POST["mdp"]) ? $_POST["mdp"] : NULL);

        $daoinscrit = new DAOInscrit();
        
        $inscrit = new Inscrit();
        $inscrit = $daoinscrit->findByLogin($login);

        foreach ($inscrit as $key => $user) {
            $inscritlog = $user->getLogin();
            $inscritmdp = $user->getMdp();
            $inscritid = $user->getId();
            $inscritrole = $user->getIdRole();

            if ($login == $inscritlog && $inscritmdp == $mdp) {
                
                $daorole = new DAORole();
                $role=$daorole->find($inscritrole);
                Session::initialiserSessionGlobale($inscritid, $inscritlog, $role);
                
                switch ($_SESSION["role"]->getNom()) {
                    case "fabricant":
                        header('Location: /fab/accueil');
                        break;
                    case "laboratoire":
                        Session::initialiserLaboratoire();
                        header('Location: /labo/accueil');
                        break;
                    case "administrateur":
                        header('Location: /admin/accueil');
                        break;
                    default:
                        header('Location: /connexion/404');
                        break;
                }
                exit();
            } else {
                echo "Erreur de connexion";
                header('Location: /connexion/accueil');
                exit();
            }
        }
    }

    public function delconnexion() {
        session_start();
        Session::detruireSession();
        header('Location: /connexion/accueil');
    }

    public function helloUser() {
        $page = Renderer::render('vousetesconnecte.php');
        echo $page;
    }

    public function quatrecentquatre() {
        $page = Renderer::render('page404.php');
        echo $page;
    }
    
    
}
