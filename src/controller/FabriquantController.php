<?php

require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCommandes.php';
require_once '../src/model/DAOInscrit.php';
require_once '../src/model/DAOKit.php';
require_once '../src/model/DAOLigneCommande.php';
require_once '../src/model/DAORole.php';
    
    
    

class FabriquantController {
    
    
    public function display() {
        $daokit=new DAOKit();
        //affichage des kits crée par le fabriquand connecté
        
        $kits=$daokit->findbyFabriquant($_SESSION["id"]);
        $page= Renderer::render('AccueilFabriquant.php', compact('kits'));
        echo $page;
    }
   
    public function newshow(){
        $page= Renderer::render('newkit.php');
        echo $page;
    }
    
    public function savenew(){
        session_start();
        $daokit=new DAOKit();
        $newkit=new Kit();
        $vide=false;
        
        //récupération du nom et enregistrement d'un nom vide si rien n'est saisi
        $_nom = htmlspecialchars(isset($_POST['nom']) ? $_POST['nom'] : NULL);
        if ($_nom==NULL){
            $_nom="";
            $vide=true;
        }
        $newkit->setNom($_nom);
        
        //récupération de la description et enregistrement d'une description vide si rien n'est saisi
        $_description = htmlspecialchars(isset($_POST['description']) ? $_POST['description'] : NULL);
        if ($_description==NULL){
            $_description="";
        }
        $newkit->setDescription($_description);
        
        //récupération du prix et enregistrement d'une quantite vide si rien n'est saisi
        $_prix = htmlspecialchars(isset($_POST['prix']) ? $_POST['prix'] : NULL);
        if ($_prix!=NULL){
            $_prix=(float)$prix;
        }
        else{
            $_prix=(float)0;
        }
        $newkit->setPrixUnitaire($_prix);
        
        //récupération de la quantite et enregistrement d'une quantite vide si rien n'est saisi
        $_quantite = htmlspecialchars(isset($_POST['qte']) ? $_POST['qte'] : NULL);
        if ($_quantite==NULL){
            echo("hola");
            $_quantite=0;
        }
        else{
            $_quantite=(int)$_quantite;
        }
        
        $newkit->setQuantite($_quantite);
        
        //récupération de l'Id du fabriquant qui crée le kit
        $newkit->setIdFab($_SESSION["id"]);
        
        if($vide==false){
            //enregistrement du kit
            $daokit->save($newkit);

            header('Location: /fab/accueil');
            exit();
        }
        else{
            
            header('Location: /fab/nouveau');
        }
    }
    
    public function updateshow($id){
        $daokit = new DAOKit();
        $monkit = new Kit();
        $monkit = $daokit->find($id);
        
        
        $page= Renderer::render('updatekit.php', compact('monkit'));
        echo $page;
    }
    
    public function saveupdate($id){
        $daokit = new DAOKit();
        
        //récuperation du kit à modifier
        $newkit=new Kit();
        $newkit=$daokit->find($id);
        
        //enregistrement de ce qui à été modifier dans le formulaire
        $_nom = htmlspecialchars(isset($_POST['nom']) ? $_POST['nom'] : NULL);
        if ($_nom!=NULL){
            $newkit->setNom((string)$_nom);
        }
        
        $_description = htmlspecialchars(isset($_POST['description']) ? $_POST['description'] : NULL);
        if ($_description!=NULL){
            $newkit->setDescription((string)$_description);
        }
        
        $_prix = htmlspecialchars(isset($_POST['prix']) ? $_POST['prix'] : NULL);
        if ($_prix!=NULL){
            $newkit->setPrixUnitaire((float)$_prix);
        }
        
        $_quantite = htmlspecialchars(isset($_POST['quantite']) ? $_POST['quantite'] : NULL);
        if ($_quantite!=NULL){
            $newkit->setQuantite($_quantite);
        }
        
        //enregistrement du kit modifier
        $daokit->update($newkit);
        
        header('Location: /fab/accueil');
        exit();
    }
    
    public function delete($id){
        
        $daokit = new DAOKit();
        $daokit->remove($id);
        header('Location: /fab/accueil');
        
    }
    
    public function commande(){
        $daolignecommande=new DAOLigneCommande();
        $leslignes=$daolignecommande->findByFab($_SESSION["id"]);
        //todo
        $page= Renderer::render('commandesrecues.php', compact('leslignes'));
        echo $page;
       
    }
    
    public function yes($id,$ref) {
        $daoligne=new DAOLigneCommande();
        $maligne=$daoligne->findone($id, $ref);
        $maligne->setIdEtat(1);
    }
    
    public function no($id,$ref) {
        $daoligne=new DAOLigneCommande();
        $maligne=$daoligne->findone($id, $ref);
        $maligne->setIdEtat(2);
    }
    
}