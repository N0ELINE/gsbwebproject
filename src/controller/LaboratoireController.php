<?php

require_once '../src/model/Panier.php';

class LaboratoireController {
    
    public function display() {
        
        $daokit=new DAOKit();
        $kits=$daokit->findAll();
        $page= Renderer::render('AccueilLaboratoire.php', compact('kits'));
        echo $page;
    }
    
     public function old(){
//        $daocommande = new DAOCommandes();
//        $daolignecommande = new DAOLigneCommande();
//        $commandes=$daocommande->findByLabo($_SESSION["id"]);
//        foreach ($commandes as $key=>$commande){
//            $lignecommande=$daolignecommande->findbycommande($commande->getReference());
//        
         
     }
     
     public function displaypanier(){
        $totalHT = 0;
        foreach($_SESSION["panier"] as $key=>$ligne){
            $totalHT=$_SESSION["panier"][$key]["prixunitaire"]*$_SESSION["panier"][$key]["quantite"]+$totalHT;
        }
        $totalTTC=$totalHT*1.2;
        $page= Renderer::render('Panier.php',compact('totalHT','totalTTC'));
        echo $page;
     }


    public function add($id){
        $daokit = new DAOKit();
        $monkit=$daokit->find($id);
        $quantite = htmlspecialchars(isset($_POST["quantite-".$id]) ? $_POST["quantite-".$id] : NULL);
        Panier::add($monkit, $quantite);
         header('Location: /labo/accueil');
        
    }
    
    public function delall($id){
        
    }
    
    public function delone($id){
        Panier::supprimer($id);
        header('Location: /labo/panier');
    }
    
    public function modify($id){
    $quantite = htmlspecialchars(isset($_POST["quantite-".$id]) ? $_POST["quantite-".$id] : NULL);
    Panier::remplacerQuantite($id,$quantite);
    header('Location: /labo/panier');
    }
    
    public function save(){
        //passer la commande
        $daocommande=new DAOCommandes();
        $daoligne=new DAOLigneCommande();
        $commande=new Commandes();
        
        $totalHT = 0;
        foreach($_SESSION["panier"] as $key=>$ligne){
            $totalHT=$_SESSION["panier"][$key]["prixunitaire"]*$_SESSION["panier"][$key]["quantite"]+$totalHT;
        }
        $commande->setIdLabo($_SESSION["id"]);
        $commande->setTotalHT($totalHT);
        $totalTTC=$totalHT*1.2;
        $commande->setTotalTTC();
        
        $maligne=new LigneCommande();
        foreach($_SESSION["panier"] as $key=>$ligne){
            $nom=$_SESSION["panier"][$key]["nom"];
            $fabriquant=$_SESSION["panier"][$key]["fabriquant"];
            $description=$_SESSION["panier"][$key]["description"];
            $prixunitaire=$_SESSION["panier"][$key]["prixunitaire"];
            $quantiteStock=$_SESSION["panier"][$key]["quantiteStock"];
            $quantite=$_SESSION["panier"][$key]["quantite"];
            $maligne=new LigneCommande();
            $maligne->setIdEtat(3);
            $maligne->setKitDescription($description);
            $maligne->setKitFabriquant($fabriquant);
            $maligne->setKitNom($nom);
            $maligne->setPrixUnitaire($prixunitaire);
            $maligne->setQuantite($quantite);
            //todo
            
        }
        
    }
    
    
}
