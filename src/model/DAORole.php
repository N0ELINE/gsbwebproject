<?php

require_once 'Inscrit.php';
require_once 'Role.php';
require_once 'singleton.php';

 
Class DAORole {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    
    //function 
    
    function find($id) : Role {
            $requete = $this->cnx -> prepare("SELECT * FROM ROLE WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject("Role");
            return $result;
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM ROLE");
            $requete -> execute();      
            $roles = array();
            while ( $result = $requete->fetchObject('Role') ){
                $roles[] = $result; 
            };
            return $roles;       
    }
    
    public function findByName(string $name) :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM ROLE WHERE Nom = :name");
            $requete -> bindValue(':name', $name, PDO::PARAM_STR);
            $requete -> execute();
            $roles = array();
            while ($result = $requete->fetchObject('Role') ){
                $roles[] = $result; 
            }; 
            return $roles;  
    }  
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM ROLE WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            
    }
    
   public function save(Role $role){
       
        $cnx=$this->cnx;
       
        $Id=$role->getId();
        $Nom=$role->getNom();
        
        //requete sql
        $SQLS="INSERT INTO ROLE (Nom) VALUES (:Nom)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":Nom",$Nom, PDO::PARAM_STR);
        $prepareStatementSave->execute();
    }
    
}