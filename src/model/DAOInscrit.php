<?php

require_once 'Inscrit.php';
require_once 'Role.php';
require_once 'singleton.php';

 
Class DAOInscrit {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }    
    
    function find($id) : object {
            $requete = $this->cnx -> prepare("SELECT * FROM INSCRIT WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject('Inscrit');
            return $result;
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM INSCRIT");
            $requete -> execute();      
            $inscrits = array();
            while ( $result = $requete->fetchObject('Inscrit') ){
                $inscrits[] = $result; 
            };
            return $inscrits;       
    }
    
    public function findByLogin(string $login) :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM INSCRIT WHERE Login = :login");
            $requete -> bindValue(':login', $login, PDO::PARAM_STR);
            $requete -> execute();
            $inscrits = array();
            while ($result = $requete->fetchObject('Inscrit') ){
                $inscrits[] = $result; 
            }; 
            return $inscrits;  
    }  
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM INSCRIT WHERE Id=:id");
            $requete->bindValue("id", $id,PDO::PARAM_INT);
            $requete -> execute();
    }
    
   public function save(Inscrit $inscrit){
       
        $cnx=$this->cnx;
       
        $Login=$inscrit->getLogin();
        $Mdp=$inscrit->getMdp();
        $IdRole=$inscrit->getIdRole();
        
        //requete sql
        $SQLS="INSERT INTO INSCRIT (Login,Mdp,IdRole) VALUES (:Login,:Mdp,:IdRole)";
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":Login",$Login, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Mdp",$Mdp, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":IdRole",$IdRole, PDO::PARAM_INT);

        $prepareStatementSave->execute();
    }
    
    public function update(Inscrit $inscrit){
        
        $cnx=$this->cnx;
       
        $id=$inscrit->getId();
        $Login=$inscrit->getLogin();
        $Mdp=$inscrit->getMdp();
        $IdRole=$inscrit->getRole();
       
        //requete sql
        $SQLU="UPDATE INSCRIT SET Login=:Login, Mdp=:Mdp, IdRole=:IdRole WHERE Id=:id";
       
        //prepare statement
        $prepareStatementUpdate=$cnx->prepare($SQLU);
        $prepareStatementUpdate->bindValue(":id",$id, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":Login",$Login, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Mdp",$Mdp, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Role",$IdRole, PDO::PARAM_INT);

        $prepareStatementUpdate->execute();
    }
    
}
