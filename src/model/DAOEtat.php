<?php

require_once 'Inscrit.php';
require_once 'Etat.php';
require_once 'singleton.php';

 
Class DAOEtat {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    
    //function 
    
    function find($id) : object {
            $requete = $this->cnx -> prepare("SELECT * FROM ETAT WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject("Etat");
            return $result;
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM ETAT");
            $requete -> execute();      
            $etats = array();
            while ( $result = $requete->fetchObject('Etat') ){
                $etats[] = $result; 
            };
            return $etats;       
    }
    
    public function findByName(string $description) :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM ETAT WHERE Description = :description");
            $requete -> bindValue(':description', $description, PDO::PARAM_STR);
            $requete -> execute();
            $etats = array();
            while ($result = $requete->fetchObject('Etat') ){
                $etats[] = $result; 
            }; 
            return $etats;  
    }  
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM ETAT WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            
    }
    
   public function save(Etat $role){
       
        $cnx=$this->cnx;
       
        $Id=$role->getId();
        $Description=$role->getDescription();
        
        //requete sql
        $SQLS="INSERT INTO ETAT (Description) VALUES (:Description)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":Description",$Description, PDO::PARAM_STR);
        $prepareStatementSave->execute();
    }
    
}