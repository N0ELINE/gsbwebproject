<?php

require_once 'Inscrit.php';
require_once 'Role.php';
require_once 'Commandes.php';
require_once 'Kit.php';

require_once 'singleton.php';

 
Class DAOKit {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    //create, read, update, delete
    
    
    function find($id) : object {
            $requete = $this->cnx -> prepare("SELECT * FROM KIT WHERE Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject("Kit");
            return $result;
    }
    
    function findbyFabriquant($id) : Array {
            $requete = $this->cnx -> prepare("SELECT * FROM KIT WHERE IdFab=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();      
            $kits = array();
            while ( $result = $requete->fetchObject('Kit') ){
                $kits[] = $result; 
            };
            return $kits;  
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM KIT");
            $requete -> execute();      
            $kits = array();
            while ( $result = $requete->fetchObject('Kit') ){
                $kits[] = $result; 
            };
            return $kits;       
    }
    
    public function findByName(string $name) :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM KIT WHERE Nom = :Nom");
            $requete -> bindValue(':Nom', $name, PDO::PARAM_STR);
            $requete -> execute();
            $kits = array();
            while ($result = $requete->fetchObject('Kit') ){
                $kits[] = $result; 
            }; 
            return $kits;  
    }  
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM KIT WHERE Id=:id");
            $requete->bindValue("id", $id,PDO::PARAM_INT);
            $requete -> execute();
    }
    
   public function save(Kit $kit){
        $cnx=$this->cnx;
        $Nom=$kit->getNom();
        $Description=$kit->getDescription();
        $PrixUnitaire=$kit->getPrixUnitaire();
        $Quantite=$kit->getQuantite();
        $IdFab=$kit->getIdFab();
        
        //requete sql
        $SQLS="INSERT INTO KIT (Nom,Description,PrixUnitaire,QuantiteEnStock,IdFab) VALUES (:Nom,:Description,:PrixUnitaire,:Quantite,:IdFab)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":Nom",$Nom, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Description",$Description, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":PrixUnitaire",$PrixUnitaire, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Quantite",$Quantite, PDO::PARAM_INT);
        $prepareStatementSave->bindValue(":IdFab",$IdFab, PDO::PARAM_INT);

        $prepareStatementSave->execute();
    }
    
    public function update(Kit $kit){
        
        $cnx=$this->cnx;
       
        $id=$kit->getId();
        $Nom=$kit->getNom();
        $Description=$kit->getDescription();
        $PrixUnitaire=$kit->getPrixUnitaire();
        $Quantite=$kit->getQuantite();
        $IdFab=$kit->getIdFab();
       
        //requete sql
        $SQLU="UPDATE KIT SET Nom=:Nom, Description=:Description, PrixUnitaire=:PrixUnitaire, QuantiteEnStock=:QuantiteEnStock, IdFab=:IdFab WHERE Id=:id";
       
        //prepare statement
        $prepareStatementUpdate=$cnx->prepare($SQLU);
        $prepareStatementUpdate->bindValue(":id",$id, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":Nom",$Nom, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Description",$Description, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":PrixUnitaire",$PrixUnitaire, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":QuantiteEnStock",$Quantite, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":IdFab",$IdFab, PDO::PARAM_INT);

        $prepareStatementUpdate->execute();
    }
    
}
