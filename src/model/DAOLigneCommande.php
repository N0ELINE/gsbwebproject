<?php
    require_once 'Inscrit.php';
    require_once 'Role.php';
    require_once 'Commandes.php';
    require_once 'LigneCommande.php';

 
Class DAOLigneCommande {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    //create, read, update, delete
    
    function findone($id,$ref) :object {
            $requete = $this->cnx -> prepare("SELECT * FROM LIGNECOMMANDE WHERE Id=:id AND Reference=:ref");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> bindValue(':ref', $ref, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject("LigneCommande");
            return $result;
    }
    
    function findbycommande($ref) : Array {
            $requete = $this->cnx -> prepare("SELECT * FROM LIGNECOMMANDE WHERE Reference=:ref");
            $requete -> bindValue(':ref', $ref, PDO::PARAM_INT);
            $requete -> execute();
            $lignecommande = array();
            while ( $result = $requete->fetchObject('LigneCommande') ){
                $lignecommande[] = $result; 
            };            
            return $lignecommande;
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM LIGNECOMMANDE");
            $requete -> execute();      
            $lignecommande = array();
            while ( $result = $requete->fetchObject('LigneCommande') ){
                $lignecommande[] = $result; 
            };
            return $lignecommande;       
    }
    
        public function findByFab($id) :Array {
            $requete = $this->cnx -> prepare("SELECT LIGNECOMMANDE.* FROM LIGNECOMMANDE, KIT WHERE KIT.IdFab = :id AND LIGNECOMMANDE.IdKit=KIT.Id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $lignecommande = array();
            while ($result = $requete->fetchObject('LigneCommande') ){
                $lignecommande[] = $result; 
            }; 
            return $lignecommande;  
    }  
    
    public function remove($id,$ref){
            $cnx=$this->cnx;    
            $SQLR="DELETE FROM LIGNECOMMANDE WHERE Id=:id and Reference=:ref";
            $preparesStatement=$cnx->prepare($SQLR);
            $preparedStatement->bindValue("id", $id,PDO::PARAM_INT);
            $preparedStatement->bindValue("ref", $ref,PDO::PARAM_INT);
            $requete -> execute();
    }
    
   public function save(LigneCommande $lignecommande){
       
        $cnx=$this->cnx;
        $KitRef=$lignecommande->getKitRef();
        $KitNom=$lignecommande->getKitNom();
        $KitDescription=$lignecommande->getKitDescription();
        $kitfabriquant=$lignecommande->getKitFabriquant();
        $PrixUnitaire=$lignecommande->getPrixUnitaire();
        $Quantite=$lignecommande->getQuantite();
        $IdEtat=$lignecommande->getIdEtat();
        
        //requete sql
        $SQLS="INSERT INTO LIGNECOMMANDE (KitNom,KitDescription,KitFabriquant,PrixUnitaire,Quantite,IdEtat) VALUES (:KitNom,:Description,:Fabriquant,:PrixUnitaire,:Quantite,:IdEtat)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":KitNom",$KitNom, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Description",$KitDescription, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Fabriquant",$kitfabriquant, PDO::PARAM_INT);
        $prepareStatementSave->bindValue(":PrixUnitaire",$PrixUnitaire, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Quantite",$Quantite, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":IdEtat",$IdEtat, PDO::PARAM_STR);
        
        $prepareStatementSave->execute();
    }
    
    public function update(LigneCommande $lignecommande){
        
        $cnx=$this->cnx;
       
        $id=$lignecommande->getIdLigne();
        $KitNom=$lignecommande->getKitNom();
        $KitDescription=$lignecommande->getKitDescription();
        $kitfabriquant=$lignecommande->getKitFabriquant();
        $PrixUnitaire=$lignecommande->getPrixUnitaire();
        $Quantite=$lignecommande->getQuantite();
        $IdEtat=$lignecommande->getIdEtat();
       
        //requete sql
        $SQLU="UPDATE INSCRIT SET KitRef=:KitRef, KitNom=:KitNom, KitDescription=:KitDescription, KitFabriquant=:Fabriquant,PrixUnitaire=:PrixUnitaire,Quantite=:Quantite,IdEtat=:IdEtat WHERE RefCommande=:Reference AND IdLigne=:IdEtat";

        //prepare statement
        $prepareStatementUpdate=$cnx->prepare($SQLU);
        $prepareStatementSave->bindValue(":KitRef",$KitRef, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":IdLigne",$id, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":KitNom",$KitNom, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Description",$KitDescription, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Fabriquant",$kitfabriquant, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":PrixUnitaire",$PrixUnitaire, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Quantite",$Quantite, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":IdEtat",$IdEtat, PDO::PARAM_STR);

        $prepareStatementUpdate->execute();
    }
    
}
