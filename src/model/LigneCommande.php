<?php

class LigneCommande {
    private $IdLigne;
    private $RefCommande;
    private $KitNom;
    private $KitDescription;
    private $KitFabriquant;
    private $PrixUnitaire;
    private $Quantite;
    private $IdEtat;
    
    function LigneCommande() {
        
    }

    function getIdLigne() {
        return $this->IdLigne;
    }

    function getRefCommande() {
        return $this->RefCommande;
    }

    function getKitNom() {
        return $this->KitNom;
    }

    function getKitDescription() {
        return $this->KitDescription;
    }

    function getQuantite() {
        return $this->Quantite;
    }

    function getIdEtat() {
        return $this->IdEtat;
    }


    function getPrixUnitaire() {
        return $this->PrixUnitaire;
    }

    function getKitFabriquant() {
        return $this->KitFabriquant;
    }

    function setKitFabriquant($KitFabriquant) {
        $this->KitFabriquant = $KitFabriquant;
    }

        
    function setPrixUnitaire($PrixUnitaire) {
        $this->PrixUnitaire = $PrixUnitaire;
    }

        
    function setKitNom($KitNom) {
        $this->KitNom = $KitNom;
    }

    function setKitDescription($KitDescription) {
        $this->KitDescription = $KitDescription;
    }

    function setQuantite($Quantite) {
        $this->Quantite = $Quantite;
    }

    function setIdEtat($IdEtat) {
        $this->IdEtat = $IdEtat;
    }


    
    
}

