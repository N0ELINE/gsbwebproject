<?php

class Kit {
    private $Id;
    private $Nom;
    private $Description;
    private $PrixUnitaire;
    private $QuantiteEnStock;
    private $IdFab;
    
    function Kit() {
        
    }
    
    function getId() {
        return $this->Id;
    }

    function getNom() {
        return $this->Nom;
    }

    function getDescription() {
        return $this->Description;
    }

    function getPrixUnitaire() {
        return $this->PrixUnitaire;
    }

    function getQuantite() {
        return $this->QuantiteEnStock;
    }

    function getIdFab() {
        return $this->IdFab;
    }


    function setNom($Nom) {
        $this->Nom = $Nom;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }

    function setPrixUnitaire($PrixUnitaire) {
        $this->PrixUnitaire = $PrixUnitaire;
    }

    function setQuantite($Quantite) {
        $this->QuantiteEnStock = $Quantite;
    }

    function setIdFab($IdFab) {
        $this->IdFab = $IdFab;
    }



    
}

