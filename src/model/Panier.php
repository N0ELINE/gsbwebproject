<?php

class Panier {
    private $masession;
    
    static function add($monkit,$quantite){
        $produitExiste=false;
        
        $listeId = array_column($_SESSION["panier"], "id");
        
        if(!in_array($monkit->getId(), $listeId)){
            $_SESSION["panier"][] = [
                'id' =>$monkit->getId(),
                'nom'=>$monkit->getNom(),
                'description'=>$monkit->getDescription(),
                'fabriquant'=>$monkit->getIdFab(),
                'prixunitaire'=>$monkit->getPrixUnitaire(),
                'quantiteStock'=>$monkit->getQuantite(),
                'quantite' => (int)$quantite
            ];
        } else {
            
          foreach($_SESSION["panier"] as $key=>$element){
           if($element['id'] == $monkit->getId()){
              $_SESSION["panier"][$key]['quantite']=$element['quantite']+(int)$quantite;
           }
        }
        }
    }
    
    static function afficherContenuPanier(){
        echo "<pre>";
        print_r($_SESSION["panier"]);
        echo("</pre>");
    }
    
    static function remplacerQuantite($id,$quantite){
        foreach ($_SESSION["panier"] as $key => $ligne) {
            if($ligne['id']==$id){
                $_SESSION["panier"][$key]["quantite"]=(int)$quantite;
                
            }
        }
    }
    
    static function supprimer($idKit){
        foreach($_SESSION["panier"] as $key=>$ligne){
            if($ligne['id']==$idKit){
                unset($_SESSION["panier"][$key]);
            }
        }
    }
    
    static function toutSupprimer(){
        unset($_SESSION["panier"]);
        
    }
        
}

