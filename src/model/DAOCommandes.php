<?php

require_once 'Inscrit.php';
require_once 'Role.php';
require_once 'Commandes.php';

 
Class DAOCommandes {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    //create, read, update, delete
    
    
    function find($id) : object {
            $requete = $this->cnx -> prepare("SELECT * FROM COMMANDES WHERE Reference=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $result = $requete->fetchObject('Commandes');
            return $result;
    }
    
    public function findAll() :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM COMMANDES");
            $requete -> execute();      
            $commande = array();
            while ( $result = $requete->fetchObject('Commandes') ){
                $commande[] = $result; 
            };
            return $commande;       
    }
    
    public function findByLabo($id) :Array {
            $requete = $this->cnx -> prepare("SELECT * FROM COMMANDES WHERE IdLabo = :id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            $commande = array();
            while ($result = $requete->fetchObject('Commandes') ){
                $commande[] = $result; 
            }; 
            return $commande;  
    }  
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM COMMANDES WHERE Reference=:id");
            $requete->bindValue("id", $id,PDO::PARAM_INT);
            $requete -> execute();
    }
    
   public function save(Commandes $commande){
       
        $cnx=$this->cnx;
       
        $idlabo=$commande->getIdLabo();
        $date=$commande->getDate();
        $TotalHT=$commande->getTotalHT();
        $TotalTTC=$commande->getTotalTTC();
        
        //requete sql
        $SQLS="INSERT INTO COMMANDES (IdLabo,Date,TotalHT,TOtalTTC) VALUES (:IdLabo,SYSDATE(),:TotalHT,:TotalTTC)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":IdLabo",$idlabo, PDO::PARAM_INT);
//        $prepareStatementSave->bindValue(":Date",$date, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":TotalHT",$TotalHT, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":TotalTTC",$TotalTTC, PDO::PARAM_STR);

        $prepareStatementSave->execute();
    }
    
    
    
    public function update(Commandes $commandes){
        
        $cnx=$this->cnx;
       
        $Reference=$commandes->getReference();
        $idlabo=$commande->getIdLabo();
        $date=$commande->getDate();
        $TotalHT=$commande->getTotalHT();
        $TotalTTC=$commande->getTotalTTC();
       
        //requete sql
        $SQLU="UPDATE COMMANDES SET IdLabo=:IdLabo, Date=:Date,TotalHT=:TotalHT,TotalTTC=:TotalTTC WHERE Reference=:Reference";
       
        //prepare statement
        $prepareStatementUpdate=$cnx->prepare($SQLU);
        $prepareStatementUpdate->bindValue(":Reference",$Reference, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":IdLabo",$IdLabo, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":Date",$date("Y-m-d H:i:s", strtotime($date)), PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":TotalHT",$TotalHT, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":TotalTTC",$TotalTTC, PDO::PARAM_STR);

        $prepareStatementUpdate->execute();
    }
    
}
