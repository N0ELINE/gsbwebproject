
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <!--<li class="nav-item">
            <a class="nav-link" href="../country/show">Pays</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../city/show">Ville</a>
          </li>-->
           <li class="nav-item">
            <?php if($_SESSION["flag"] == true) { ?>
               <a class="nav-link" href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a>
            <?php          }          else {              ?>
              <a class="nav-link" href="../connexion/accueil">Connexion</a>
            <?php          }          ?>
            
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>