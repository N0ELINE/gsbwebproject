<?php //session_start();?>
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../labo/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="../labo/accueil">Tous les produits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../labo/accueil">Anciennes Commandes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../labo/panier">Panier</a>
          </li>
           <li class="nav-item">
               <a class="nav-link"href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a>
            
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
        
    
        
        <div>
            <table>
                <tr>
                    <th> Nom du Kit vendu---</th>
                    <th> Description du Kit proposé--- </th>
                    <th> Prix Unitaire--- </th>
                    <th> Quantité en Stock---</th>
                    <th> Quantité à ajouter au panier</th>
                </tr>
                    <?php foreach ($kits as $key=>$kit){ ?>
                <tr> 
                    <td><?php print($kit->getNom());?></td>
                    <td><?php print($kit->getDescription()); ?> </td>
                    <td><?php print($kit->getPrixUnitaire()); ?> </td>
                    <td><?php print($kit->getQuantite());?></td>
                    
                   <form method="post" action="../labo/ajouter/<?php  print($kit->getId());?>">
                            <td><input value="5" type="text" class="form-control" name="quantite-<?php  print($kit->getId());?>" id="quantite" required></td>   
                        
                            <td><a href="/labo/ajouter/<?php  print($kit->getId());?>">
                                <button type="submit" class="btn btn-info " >Ajouter au Panier</button>
                            </a></td>
                    </form>
                    
                </tr>       
                <?php  } ?>
            </table>
            </br></br>
            
        </div>
    </body>
</html>
