<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../labo/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="../labo/accueil">Tous les produits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../labo/accueil">Anciennes Commandes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../labo/panier">Panier</a>
          </li>
           <li class="nav-item">
               <a class="nav-link"href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a>
            
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
        
        <div>
            
            <table>
                <tr>
                    <th> Nom du Kit---</th>
                    <th> Description du Kit----------</th>
                    <th> Prix Unitaire--- </th>
                    <th> Quantité en Stock---</th>
                    <th> Quantité Commandée</th>
                </tr>
               
                    <?php foreach ($_SESSION["panier"] as $key=>$ligne): ?>
                <tr>
                    
                    <td><?php echo $_SESSION["panier"][$key]['nom']; ?></td>
                    <td><?php echo $_SESSION["panier"][$key]['description']; ?></td>
                    <td><?php echo $_SESSION["panier"][$key]['prixunitaire']; ?></td>
                    <td><?php echo $_SESSION["panier"][$key]['quantiteStock'];?></td>
                    <td><?php echo $_SESSION["panier"][$key]['quantite'];?></td>
                    
                    
                   <form method="post" action="../labo/modifier/<?php  echo $_SESSION["panier"][$key]['id']; ?>">
                            <td><input type="text" class="form-control" name="quantite-<?php  print($ligne['id']);?>" id="quantite" required></td>   
                        
                            <td><a href="/labo/modifier/<?php echo $_SESSION["panier"][$key]['id'];  ?>">
                                <button type="submit" class="btn btn-secondary"  placeholder="<?php //echo quantité commandée ?>" >Valider la nouvelle quantité</button>
                            </a></td>
                    </form>
                <form>
                    <td><a href="/labo/supprimerun/<?php  echo $_SESSION["panier"][$key]['id']; ?>">
                                <button type="button" class="btn btn-danger" >Supprimer</button>
                            </a></td>
                </form>
                    
                </tr>  
                <?php endforeach; ?>
            </table>
            </br></br>
            </br></br>
            <p>Total HT: <?php  echo $totalHT?></p>
            </br>
            <p>Total TTC: <?php  echo $totalTTC?></p>
            <p>Application d'une taxe de 20%</p>
            
            </br></br>
            
            <form>
                        <a type="submit" class="btn btn-success" href="/labo/commander">Commander</a>
            </form>
            
            </br></br>
        </div>
        
    </body>
</html>
