
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../fab/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../fab/accueil">Gestion des Kits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../fab/commandesrecues">Commandes reçues</a>
          </li>
           <li class="nav-item">
               <a class="nav-link"href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a> 
           
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
        
    
    
    
        <div>
            <a href="/fab/nouveau"><button type="button" class="btn btn-info" href="/fab/nouveau">Enregistrer un nouveau kit &#x1F5C7;</button></a><br><br><br>
        </div>
        
        <div><!-- AFFICHAGE DES KITS SOUS FORME DE TABLEAU-->
            <table>
                <tr>
                    <th> Nom du Kit vendu---</th>
                    <th> Description du Kit proposé--- </th>
                    <th> Prix Unitaire--- </th>
                    <th> Quantité en Stock</th>
                </tr>
                    <?php foreach ($kits as $key=>$kit){ ?>
                <tr> 
                    <td><?php print($kit->getNom());?></td>
                    <td><?php print($kit->getDescription()); ?> </td>
                    <td><?php print($kit->getPrixUnitaire()); ?> </td>
                    <td><?php print($kit->getQuantite());?></td>

                    <td><a href="/fab/modifier/<?php  print($kit->getId());?>">
                        <button type="button" class="btn btn-warning" >Modifier &#x1F589;</button>
                    </a></td>
                    <td><a href="/fab/supprimer/<?php  print($kit->getId()); ?>">
                    <button type="button" class="btn btn-danger">Supprimer &#x1F5D9;</button>
                    </a></td>
                </tr>       
                <?php  } ?>
            </table><!-- FIN AFFICHAGE DES KITS -->
            </br></br>
            
        </div>
    </body>
</html>
