
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        
           <li class="nav-item">
            <?php if($_SESSION["flag"] == true) { ?>
               <a class="nav-link" href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a>
            <?php          }          else {              ?>
              <a class="nav-link" href="../connexion/accueil">Connexion</a>
            <?php          }          ?>
            
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
    
        <div>
            <p>
                Modifiez seulement les champs que vous souhaitez modifier, vous pouvez laisser les autres vides
            </p>
            <br>
        </div>
        <div>
            
            <form method="POST" action="../../fab/enregistrementmodifier/<?php print($monkit->getId()); ?>">
                <div class="form-group">
                    <label for="Nom">Nom du kit à enregistrer</label>
                    <input type="nom" class="form-control" id="nom" name="nom" placeholder="<?php echo $monkit->getNom(); ?>" >
                </div>
                <div class="form-group">
                  <label for="Description">Description du kit</label>
                  <input type="description" class="form-control" id="description" name="description" placeholder="<?php echo $monkit->getDescription(); ?>" >
                </div>
                <div class="form-group">
                  <label for="Prix">Prix Unitaire</label>
                  <input type="prix" class="form-control" id="prix" name="prix" placeholder="<?php echo $monkit->getPrixUnitaire(); ?>" >
                </div>
                <div class="form-group">
                  <label for="Quantite">Quantite en stock</label>
                  <input type="quantite" class="form-control" id="quantite" name="quantite" placeholder="<?php echo $monkit->getQuantite(); ?>" >
                </div>
                              
                <button type="Enregistrer" class="btn btn-primary">Enregistrer</button>
            </form>
        </div> 
       
    </body>
</html>
