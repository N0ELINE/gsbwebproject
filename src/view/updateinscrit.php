
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../admin/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>
    </br>
        
    <div>
            <p>
                Modifiez seulement les champs que vous souhaitez modifier, vous pouvez laisser les autres vides
            </p>
            <br>
        </div>
        <div>
            
            <form method="POST" action="../../admin/enregistrermodifier/<?php print($moninscrit->getId()); ?>">
                <div class="form-group">
                    <label for="Nom">Login de l'inscrit</label>
                    <input type="nom" class="form-control" id="nom" name="nom" placeholder="<?php echo $moninscrit->getLogin(); ?>" >
                </div>
                <div class="form-group">
                  <label for="Mdp">Nouveau mot de passe pour l'inscrit</label>
                  <input type="mdp" class="form-control" id="mdp" name="mdp"  >
                </div>
                <div class="form-check">
                <input type="checkbox" class="form-check-input" id="materialUnchecked">
                    <label class="form-check-label" name="modifier" for="materialUnchecked">Souhaitez vous modifier le role</label>
                </div>
               <select class=form-control nam="role">
                    <option value=1>Laboratoire</option>
                    <option value=2>Fabriquant</option>
                    <option value=3>Administrateur</option>
                </select>
                              
                <button type="Enregistrer" class="btn btn-primary">Enregistrer</button>
            </form>
        </div> 
    
    </body>
</html>
