
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../fab/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../fab/accueil">Gestion des Kits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../fab/commandesrecues">Commandes reçues</a>
          </li>
           <li class="nav-item">
               <a class="nav-link"href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a> 
           
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
        
    
    
    
        <div>
            <a href="/fab/nouveau"><button type="button" class="btn btn-info" href="/fab/nouveau">Enregistrer un nouveau kit &#x1F5C7;</button></a><br><br><br>
        </div>
        
        <div><!-- AFFICHAGE DES KITS SOUS FORME DE TABLEAU-->
            <table>
                <tr>
                    <th> Nom du Kit commandé---</th>
                    <th> Prix Unitaire du Kit--- </th>
                    <th> Quantité commandée--- </th>
                    <th> Quantité en Stock</th>
                </tr>
                    <?php foreach ($leslignes as $key=>$ligne){ ?>
                <tr> 
                    <td><?php print($kit->getKitNom());?></td>
                    <td><?php print($kit->getPrixUnitaire()); ?> </td>
                    <td><?php print($kit->getGetQuantité()); ?> </td>
                    <td><?php print($kit->getQuantite());?></td>

                    <td><a href="/fab/refuser/<?php  print($ligne->getId());?>/<?php  print($ligne->getRefCommande());?>">
                        <button type="button" class="btn btn-warning" >Refuser</button>
                    </a></td>
                    <td><a href="/fab/accepter/<?php  print($ligne->getId()); ?>/<?php  print($ligne->getRefCommande());?>">
                    <button type="button" class="btn btn-success">Accepter</button>
                    </a></td>
                </tr>       
                <?php  } ?>
            </table><!-- FIN AFFICHAGE DES KITS -->
            </br></br>
            
        </div>
    </body>
</html>
