
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion des Commandes et des Stocks</title>


  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/index.css" rel="stylesheet">

</head>

<body>
    
    <?php session_start();?>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../admin/accueil">Gestion des Commandes et des Stocks</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            
           <li class="nav-item">
               <a class="nav-link"href="../connexion/hellouser"> Hello <?php echo $_SESSION["login"]; ?></a>
            
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </br>
        
    
    
    
        <div>
            <a href="/admin/nouveau"><button type="button" class="btn btn-info" href="/admin/nouveau">Enregistrer un nouvel inscrit &#x1F5C7;</button></a><br><br><br>
        </div>

        <div>
            <table>
                <tr>
                    <th> Login de l'inscrit---</th>
                    <th> Role de l'inscrit</th>
                </tr>
                    <?php foreach ($inscrits as $key=>$inscrit){ ?>
                <tr> 
                    <td><?php print($inscrit->getLogin());?></td>
                    <td><?php switch ($inscrit->getIdRole()) {
                                case 1:
                                    echo 'Laboratoire';
                                    break;
                                case 2:
                                    echo 'Fabriquant';
                                    break;
                                case 3:
                                    echo 'Administrateur';
                                    break;
                                default:
                                    break;
                            } ?> </td>
                    <td><a href="/admin/modifier/<?php  print($inscrit->getId());?>">
                        <button type="button" class="btn btn-warning" >Modifier &#x1F589;</button>
                    </a></td>
                    <td><a href="/admin/supprimer/<?php  print($inscrit->getId()); ?>">
                    <button type="button" class="btn btn-danger">Supprimer &#x1F5D9;</button>
                    </a></td>
                </tr>       
                <?php  } ?>
            </table>
            </br></br>
            
        </div>
    </body>
</html>
