-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: GSBWeb
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `COMMANDES`
--

DROP TABLE IF EXISTS `COMMANDES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMMANDES` (
  `Reference` int(11) NOT NULL AUTO_INCREMENT,
  `IdLabo` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `TotalHT` double DEFAULT NULL,
  `TotalTTC` double DEFAULT NULL,
  PRIMARY KEY (`Reference`),
  KEY `IdLabo` (`IdLabo`),
  CONSTRAINT `COMMANDES_ibfk_1` FOREIGN KEY (`IdLabo`) REFERENCES `INSCRIT` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMMANDES`
--

LOCK TABLES `COMMANDES` WRITE;
/*!40000 ALTER TABLE `COMMANDES` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMMANDES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ETAT`
--

DROP TABLE IF EXISTS `ETAT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ETAT` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ETAT`
--

LOCK TABLES `ETAT` WRITE;
/*!40000 ALTER TABLE `ETAT` DISABLE KEYS */;
INSERT INTO `ETAT` VALUES (1,'Acceptée'),(2,'Refusée'),(3,'En attente');
/*!40000 ALTER TABLE `ETAT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `INSCRIT`
--

DROP TABLE IF EXISTS `INSCRIT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INSCRIT` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) NOT NULL,
  `Mdp` varchar(250) NOT NULL,
  `IdRole` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdRole` (`IdRole`),
  CONSTRAINT `INSCRIT_ibfk_1` FOREIGN KEY (`IdRole`) REFERENCES `ROLE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INSCRIT`
--

LOCK TABLES `INSCRIT` WRITE;
/*!40000 ALTER TABLE `INSCRIT` DISABLE KEYS */;
INSERT INTO `INSCRIT` VALUES (1,'laboratoire','123+aze',1),(2,'fabricant','123+aze',2),(3,'admin','123+aze',3);
/*!40000 ALTER TABLE `INSCRIT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KIT`
--

DROP TABLE IF EXISTS `KIT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KIT` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `PrixUnitaire` double DEFAULT NULL,
  `QuantiteEnStock` int(11) DEFAULT NULL,
  `IdFab` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdFab` (`IdFab`),
  CONSTRAINT `KIT_ibfk_1` FOREIGN KEY (`IdFab`) REFERENCES `INSCRIT` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KIT`
--

LOCK TABLES `KIT` WRITE;
/*!40000 ALTER TABLE `KIT` DISABLE KEYS */;
INSERT INTO `KIT` VALUES (1,'Kit1','Kit d analyse sanguine',5,0,2),(2,'Kit2','Kit d’analyse d’urine',1.32,0,2),(3,'Kit3','Kit d’analyse salivaire',10,0,2),(4,'Kit4','Kit d’analyse basique',50,12,2);
/*!40000 ALTER TABLE `KIT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LIGNECOMMANDE`
--

DROP TABLE IF EXISTS `LIGNECOMMANDE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LIGNECOMMANDE` (
  `IdLigne` int(11) NOT NULL,
  `RefCommande` int(11) NOT NULL,
  `IdFabriquant` int(11) DEFAULT NULL,
  `KitNom` varchar(20) DEFAULT NULL,
  `KitDescription` varchar(250) DEFAULT NULL,
  `KitFabriquant` int(11) DEFAULT NULL,
  `PrixUnitaire` double DEFAULT NULL,
  `Quantite` int(11) DEFAULT NULL,
  `IdEtat` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdLigne`,`RefCommande`),
  KEY `RefCommande` (`RefCommande`),
  KEY `IdEtat` (`IdEtat`),
  KEY `IdFabriquant` (`IdFabriquant`),
  CONSTRAINT `LIGNECOMMANDE_ibfk_1` FOREIGN KEY (`RefCommande`) REFERENCES `COMMANDES` (`Reference`),
  CONSTRAINT `LIGNECOMMANDE_ibfk_2` FOREIGN KEY (`IdEtat`) REFERENCES `ETAT` (`Id`),
  CONSTRAINT `LIGNECOMMANDE_ibfk_3` FOREIGN KEY (`IdFabriquant`) REFERENCES `INSCRIT` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LIGNECOMMANDE`
--

LOCK TABLES `LIGNECOMMANDE` WRITE;
/*!40000 ALTER TABLE `LIGNECOMMANDE` DISABLE KEYS */;
/*!40000 ALTER TABLE `LIGNECOMMANDE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE`
--

DROP TABLE IF EXISTS `ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ROLE` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE`
--

LOCK TABLES `ROLE` WRITE;
/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` VALUES (1,'laboratoire'),(2,'fabricant'),(3,'administrateur');
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'GSBWeb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-13  9:52:03
